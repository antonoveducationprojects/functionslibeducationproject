using System;

namespace FunctionsLib
{
    /// <summary>
    /// Implements addition operation as Binares Function
    /// </summary>
    public sealed class Addition : Binares
    {
        public Addition(Function l, Function r)
            : base(l, r)
        {
        }

        public override double Calc(double x)
        {
            throw new NotImplementedException();
        }

        public override Function Diff()
        {
            throw new NotImplementedException();
        }
    }
}