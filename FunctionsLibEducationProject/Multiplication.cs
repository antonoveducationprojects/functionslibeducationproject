using System;

namespace FunctionsLib
{
    /// <summary>
    /// Implements multiplication operation as Binares Function
    /// </summary>
    public sealed class Multiplication : Binares
    {
        public Multiplication(Function l, Function r)
            : base(l, r)
        {
        }

        public override double Calc(double x)
        {
            throw new NotImplementedException();
        }

        public override Function Diff()
        {
            throw new NotImplementedException();
        }
    }
}