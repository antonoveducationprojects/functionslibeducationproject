using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionsLib
{
    /// <summary>
    /// Implements division operation as Binares Function.
    /// </summary>
    public sealed class Division : Binares
    {
        public Division(Function l, Function r)
            : base(l, r)
        {
        }

        public override double Calc(double x)
        {
            throw new NotImplementedException();
        }

        public override Function Diff()
        {
            throw new NotImplementedException();
        }
    }
}