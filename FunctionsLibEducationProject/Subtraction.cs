using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionsLib
{
    /// <summary>
    /// Implements substraction operation as Binares Function
    /// </summary>
    public sealed class Subtraction : Binares
    {
        public Subtraction(Function l, Function r)
            : base(l, r)
        {
        }

        public override double Calc(double x)
        {
            throw new NotImplementedException();
        }

        public override Function Diff()
        {
            throw new NotImplementedException();
        }
    }
}