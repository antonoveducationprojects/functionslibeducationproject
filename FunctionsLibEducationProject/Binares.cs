using System;
using System.Collections.Generic;

namespace FunctionsLib
{
    /// <summary>
    /// Implements all common operations for binares Functions and operators
    /// </summary>
    public abstract class Binares : Function
    {
        protected Function leftArg;
        protected Function rightArg;
        protected Func<double, double, double> funcPtr;

        // protected static Dictionary<string,char> operationSymbols=new Dictionary<string,char>();

        /// <summary>
        /// Constructor with two parameters that realises Dependency Injection of Functions.
        /// </summary>
        /// <param name="l">Function corresponding left operand.</param>
        /// <param name="r">Function corresponding right operand.</param>
        protected Binares(Function l, Function r)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Default implementation for all Binares Function.
        /// </summary>
        /// <returns>string - text representation of Binares Function.</returns>
        public override string ToString()
        {
            throw new NotImplementedException();
        }
    }
}